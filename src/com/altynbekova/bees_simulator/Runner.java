package com.altynbekova.bees_simulator;

import com.altynbekova.bees_simulator.entity.*;
import com.altynbekova.bees_simulator.service.BeeService;
import com.altynbekova.bees_simulator.util.PropertyManager;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;

public class Runner {
    private static final String SIMULATION_TIME_SECONDS_KEY = "simulation.time";

    public static synchronized void printCoordinates(Bee bee, int totalTime, Point start, Point end, Point current, int time) {
        System.out.println(MessageFormat.format("{0}[id={1}]. Total time {2}. Start {3}. End {4}. Current coordinates are {5} at the {6} second",
                bee.getClass().getSimpleName(), bee.getId(), totalTime, start, end, current, time));
    }

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        try {
            Date checkTime;
            BeeService beeService = new BeeService();
            SortedMap<Bee, Boolean> aliveBees;
            String beeType;
            int simulationTime=Integer.parseInt(PropertyManager.getInstance().getProperty(SIMULATION_TIME_SECONDS_KEY));

            beeService.runSimulation();
            checkTime = new Date();
            Thread.sleep(beeService.getMaxPeriodicity()*1000);

            aliveBees = beeService.getAliveBees(checkTime, true);
            System.out.println("\nList of alive bees:");
            for (Bee bee : aliveBees.keySet()) {
                System.out.println(bee);
            }

            System.out.println("\nTotal amount of generated bees: " + beeService.generatedBeesAmount());
            System.out.println("drone bees: " + beeService.generatedDronesAmount());
            System.out.println("worker bees: " + beeService.generatedWorkersAmount());
            System.out.println("Simulation time: " + simulationTime + " seconds");

            System.out.println("Enter a type name of the bee for getting list of alive bees by type. Available types are 'Worker', 'Drone'");
            beeType = br.readLine().toLowerCase();
            switch (beeType) {
                case "worker":
                    System.out.println("Amount of alive generated workers: " + beeService.getAliveBeesAmount(checkTime, true, Bee.Type.WORKER));
                    break;
                case "drone":
                    System.out.println("Amount of alive generated drones: " + beeService.getAliveBeesAmount(checkTime, true, Bee.Type.DRONE));
                    break;
                default:
                    System.out.println("Amount of alive bees: " + aliveBees.keySet().size());
            }

            beeService.serializeBees(aliveBees);
            Thread.sleep(3000);

            SortedMap<Bee, Boolean> deserializedBees = beeService.deserializeBees();
            System.out.println("\nList of deserialized alive bees:");
            for (Object deserializedBee : deserializedBees.keySet()) {
                System.out.println(deserializedBee);
            }

            beeService.startMovingWorkers();
            beeService.startMovingDrones();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }
}
