package com.altynbekova.bees_simulator.service;

import com.altynbekova.bees_simulator.ServiceException;
import com.altynbekova.bees_simulator.entity.*;
import com.altynbekova.bees_simulator.util.PropertyManager;

import java.io.*;
import java.util.*;

public class BeeService {
    public static final String SIMULATION_TIME_SECONDS_KEY = "simulation.time";
    private static final int ZERO_SEC = 0;
    private static final String SERIALIZATION_FILE_NAME = "BeeObjects.ser";
    private static final String DRONE_BIRTH_PERIODICITY_KEY = "bee.drone.birth.periodicity";
    private static final String DRONE_BIRTH_COEFFICIENT_KEY = "bee.drone.birth.coefficient";
    private static final String WORKER_BIRTH_PERIODICITY_KEY = "bee.worker.birth.periodicity";
    private static final String WORKER_BIRTH_PROBABILITY_KEY = "bee.worker.birth.probability";
    private Habitat dronesHabitat;
    private Habitat workersHabitat;
    private int[] beesAmount={0,0}; //1st elem - drones, 2nd - workers

    public BeeService() {

    }

    public void runSimulation() throws IOException, InterruptedException {
        PropertyManager propertyManager = PropertyManager.getInstance();
        Timer timer = new Timer();
        int simulationTime = Integer.parseInt(propertyManager.getProperty(SIMULATION_TIME_SECONDS_KEY));
        int droneBirthPeriod = Integer.parseInt(propertyManager.getProperty(DRONE_BIRTH_PERIODICITY_KEY));
        int workerBirthPeriod = Integer.parseInt(propertyManager.getProperty(WORKER_BIRTH_PERIODICITY_KEY));
        dronesHabitat = new DronesHabitat(droneBirthPeriod,
                Double.parseDouble(propertyManager.getProperty(DRONE_BIRTH_COEFFICIENT_KEY)));
        workersHabitat = new WorkersHabitat(workerBirthPeriod,
                Double.parseDouble(propertyManager.getProperty(WORKER_BIRTH_PROBABILITY_KEY)));

        timer.scheduleAtFixedRate(dronesHabitat, ZERO_SEC, droneBirthPeriod * 1000);
        timer.scheduleAtFixedRate(workersHabitat, ZERO_SEC, workerBirthPeriod * 1000);
        Thread.sleep(simulationTime * 1000);
        timer.cancel();
    }

    public SortedMap<Bee, Boolean> getAliveBees(Date time, boolean isAlive) {
        SortedMap<Bee, Boolean> aliveBees = new TreeMap<>(Bee.BIRTH_TIME_ORDER);
        for (Bee bee : Habitat.getBees()) {
            Date deathTime = new Date(bee.getBirthTime().getTime() + bee.getLifeTime() * 1000);
            if (deathTime.compareTo(time) > 0 && isAlive)
                aliveBees.put(bee, true);
        }

        return aliveBees;
    }

    public int getAliveBeesAmount(Date time, boolean isAlive, Bee.Type type) {
        int result = 0;
        SortedMap<Bee, Boolean> aliveBees = getAliveBees(time, isAlive);
        for (Bee bee : aliveBees.keySet()) {
            if (bee.getType() == type)
                result++;
        }

        return result;
    }

    public int generatedBeesAmount() {
        return Habitat.getBees().size();
    }

    public int generatedDronesAmount() {
        if (beesAmount[0] == 0) {
            beesAmount();
        }
        return beesAmount[0];
    }

    public int generatedWorkersAmount() {
        if (beesAmount[1] == 0) {
            beesAmount();
        }
        return beesAmount[1];
    }

    private void beesAmount() {
        for (Bee bee : Habitat.getBees()) {
            if (bee.getType() == Bee.Type.DRONE)
                beesAmount[0]++;
            else
                beesAmount[1]++;
        }
    }

    public void serializeBees(SortedMap<Bee, Boolean> bees) throws ServiceException {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(SERIALIZATION_FILE_NAME, false))) {
            outputStream.writeObject(bees);
            outputStream.flush();
        } catch (FileNotFoundException e) {
            throw new ServiceException("File not found:" + SERIALIZATION_FILE_NAME, e);
        } catch (IOException e) {
            throw new ServiceException("IOException:" + SERIALIZATION_FILE_NAME, e);
        }
    }

    public SortedMap<Bee, Boolean> deserializeBees() throws ServiceException {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(SERIALIZATION_FILE_NAME))) {

            return (SortedMap<Bee, Boolean>) inputStream.readObject();

        } catch (FileNotFoundException e) {
            throw new ServiceException("File not found:" + SERIALIZATION_FILE_NAME, e);
        } catch (IOException e) {
            throw new ServiceException("IOException:" + SERIALIZATION_FILE_NAME, e);
        } catch (ClassNotFoundException e) {
            throw new ServiceException("CassNotFound", e);
        }

    }

    public void startMovingWorkers() {
        Thread threadOfWorkers;
        List<WorkerBee> workerBees = new ArrayList<>(workersHabitat.getWorkersAmount());
        for (Bee generatedBee : Habitat.getBees()) {
            if (generatedBee.getType() == Bee.Type.WORKER)
                workerBees.add((WorkerBee) generatedBee);
        }
        if (!workerBees.isEmpty()) {
            threadOfWorkers = new Thread(new WorkerAI(workerBees));
            System.out.println("\nSimulation of workers moving started");
            threadOfWorkers.start();
        }
    }

    public void startMovingDrones() {
        Thread threadOfDrones;
        List<DroneBee> droneBees = new ArrayList<>(dronesHabitat.getDronesAmount());
        for (Bee generatedBee : Habitat.getBees()) {
            if (generatedBee.getType() == Bee.Type.DRONE)
                droneBees.add((DroneBee) generatedBee);
        }

        if (!droneBees.isEmpty()) {
            threadOfDrones = new Thread(new DroneAI(droneBees));
            System.out.println("\nSimulation of drones moving started");
            threadOfDrones.start();
        }
    }

    public int getMaxPeriodicity(){
        int dronePeriodicity=0;
        int workerPeriodicity=0;
        List<Bee> bees;
        bees=Habitat.getBees();
        for (int i = 0; i < bees.size(); i++) {
            if (bees.get(i).getType()== Bee.Type.DRONE) {
                dronePeriodicity = bees.get(i).getBirthPeriodicity();
                break;
            }
        }

        for (int i = 0; i < bees.size(); i++) {
            if (bees.get(i).getType()== Bee.Type.WORKER) {
                workerPeriodicity = bees.get(i).getBirthPeriodicity();
                break;
            }
        }

        return dronePeriodicity>workerPeriodicity?dronePeriodicity:workerPeriodicity;

    }

}
