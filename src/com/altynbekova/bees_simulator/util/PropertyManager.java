package com.altynbekova.bees_simulator.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyManager {
    private static final String PROPERTIES_FILE = "resources/app.properties";
    private static PropertyManager instance;
    private final Properties properties = new Properties();

    private PropertyManager() throws IOException {
        InputStream in = PropertyManager.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);
        try {
            if (in != null)
                properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null)
                in.close();
        }
    }

    public static PropertyManager getInstance() throws IOException {
        if (instance == null) {
            instance = new PropertyManager();
        }
        return instance;
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}