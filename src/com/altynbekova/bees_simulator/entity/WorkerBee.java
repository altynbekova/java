package com.altynbekova.bees_simulator.entity;

import java.util.Date;
import java.util.UUID;

public class WorkerBee extends Bee {
    private double birthProbability;

    public WorkerBee() {
    }

    public WorkerBee(UUID id, Date birthTime, int birthPeriodicity, int lifeTime, double birthProbability) {
        super(id, birthPeriodicity, birthTime, lifeTime);
        this.birthProbability = birthProbability;
        this.setType(Type.WORKER);
    }

    public double getBirthProbability() {
        return birthProbability;
    }

    public void setBirthProbability(double birthProbability) {
        this.birthProbability = birthProbability;
    }

    @Override
    public String toString() {
        return "WorkerBee{id=" + getId() + ", birthTime=" + getBirthTime() + ", birthPeriodicity=" + getBirthPeriodicity() +
                ", lifeTime=" + getLifeTime() + " seconds, birthProbability=" + getBirthProbability() + "}";
    }
}
