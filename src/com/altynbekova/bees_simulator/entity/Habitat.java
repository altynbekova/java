package com.altynbekova.bees_simulator.entity;

import java.util.*;

public abstract class Habitat extends TimerTask{
    protected static List<Bee> bees=new ArrayList<>();
    private static SortedMap<Bee,Boolean> aliveBees=new TreeMap<>(Bee.BIRTH_TIME_ORDER);
    protected int dronesAmount=0;
    protected int workersAmount=0;


    protected Random rnd=new Random();

    private static Set<UUID> idSet=new HashSet<>();

    public static List<Bee> getBees() {
        return bees;
    }

    protected UUID generateId(){
        UUID generatedId=UUID.randomUUID();

        while(idSet.contains(generatedId)){
            generatedId=UUID.randomUUID();
        }
        idSet.add(generatedId);

        return generatedId;
    }

        public static SortedMap<Bee,Boolean> getAliveBees(Date time, boolean isAlive){
        System.out.println("\nSearching for alive bees on "+time);
        for (Bee bee : bees) {
            Date deathTime = new Date(bee.getBirthTime().getTime() + bee.getLifeTime()*1000);
            if (deathTime.compareTo(time)>0)
                aliveBees.put(bee,isAlive);
        }

        return aliveBees;
    }

    public int getDronesAmount(){
        return dronesAmount;
    }

    public int getWorkersAmount() {
        return workersAmount;
    }

    protected abstract Bee generateNewBee(int lifeTime);
}
