package com.altynbekova.bees_simulator.entity;

import com.altynbekova.bees_simulator.util.PropertyManager;

import java.io.IOException;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class WorkersHabitat extends Habitat {
    private int workersBirthPeriodicity;
    private double workersBirthProbability;

    public WorkersHabitat() {
    }

    public WorkersHabitat(int workersBirthPeriodicity, double workersBirthProbability) {
        this.workersBirthPeriodicity = workersBirthPeriodicity;
        this.workersBirthProbability = workersBirthProbability;
    }

    @Override
    public WorkerBee generateNewBee(int lifeTime) {
        UUID id = generateId();
        return new WorkerBee(id, new Date(), workersBirthPeriodicity, lifeTime, workersBirthProbability);
    }

    @Override
    public void run() {
        try {
            int maxLifeTime = Integer.parseInt(PropertyManager.getInstance().getProperty("simulation.time"));

            Random random = new Random();
            double randomValue = random.nextDouble();
            if (randomValue <= workersBirthProbability) {
                System.out.println(new Date());
                WorkerBee bee = generateNewBee(rnd.nextInt(maxLifeTime + 1));
                System.out.println(bee + " with randomValue=" + randomValue + " is generated}\n");

                bees.add(bee);
                workersAmount++;
            } else
                System.out.println(new Date() + "\nCouldn't generate a worker bee with randomValue=" + randomValue + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
