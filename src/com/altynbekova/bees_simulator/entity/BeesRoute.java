package com.altynbekova.bees_simulator.entity;

public class BeesRoute<T extends Bee> {
    private T bee;
    private Point startPoint=new Point();
    private Point endPoint=new Point();
    private int totalTime;
    private boolean isFinished;

    public BeesRoute() {
    }

    public BeesRoute(T bee, Point startPoint, Point endPoint, int totalTime) {
        this.bee = bee;
        this.startPoint = startPoint;
        this.endPoint = endPoint;
        this.totalTime=totalTime;

    }

    public T getBee() {
        return bee;
    }

    public void setBee(T bee) {
        this.bee = bee;
    }

    public Point getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Point startPoint) {
        this.startPoint.setX(startPoint.getX());
        this.startPoint.setY(startPoint.getY());
    }

    public Point getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Point endPoint) {
        this.endPoint.setX(endPoint.getX());
        this.endPoint.setY(endPoint.getY());
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }
}
