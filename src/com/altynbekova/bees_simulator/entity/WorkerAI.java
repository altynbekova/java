package com.altynbekova.bees_simulator.entity;

import com.altynbekova.bees_simulator.Runner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WorkerAI extends BaseAI<WorkerBee> {
    private static final int VELOCITY = 1; //pixel per second
    private List<WorkerBee> workerBees;
    private List<BeesRoute<WorkerBee>> beesRoutes;
    private Random rnd = new Random();

    public WorkerAI() {
    }

    public WorkerAI(List<WorkerBee> workerBees) {
        this.workerBees = workerBees;
        init();
    }

    private int getTotalTime(Point p1, Point p2) {
        if (p1.getX() == p2.getX() && p1.getY() == p2.getY())
            return 0;
        else {
            int dx = p2.getX() - p1.getX();
            int dy = p2.getY() - p1.getY();
            double dxNormalized = dx / Math.sqrt(dx * dx + dy * dy);
            return (int) Math.ceil(dx / (VELOCITY * dxNormalized));
        }
    }

    private void init() {
        beesRoutes = new ArrayList<>(workerBees.size());
        for (WorkerBee workerBee : workerBees) {
            BeesRoute<WorkerBee> workersRoute = new BeesRoute<>();
            Point startPoint = new Point(rnd.nextInt(N + 1), rnd.nextInt(N + 1));
            Point endPoint = destinationPoints[rnd.nextInt(destinationPoints.length)];

            workersRoute.setBee(workerBee);
            workersRoute.setStartPoint(startPoint);
            workersRoute.setEndPoint(endPoint);
            workersRoute.setTotalTime(getTotalTime(workersRoute.getStartPoint(), workersRoute.getEndPoint()));
            workersRoute.setFinished(false);

            beesRoutes.add(workersRoute);
        }
    }


    //moves to a corner and come back
    //(xc;yc)=(x1;y1)+(dx,dy)*V*T
    @Override
    protected void move() {
        int routesSize = beesRoutes.size();
        boolean isMoving = (routesSize > 0);
        int time = 1;
        int finishedRoutesCount = 0;
        int reversedCount = 0;
        int maxTime = Integer.MAX_VALUE;
        while (isMoving) {
            for (BeesRoute<WorkerBee> beesRoute : beesRoutes) {

                int routeTotalTime = beesRoute.getTotalTime();
                Point currentPoint = null;
                if (!beesRoute.isFinished()) {
                    currentPoint = getCurrentPoint(beesRoute.getStartPoint(), beesRoute.getEndPoint(), time, VELOCITY);
                    Runner.printCoordinates(beesRoute.getBee(), routeTotalTime, beesRoute.getStartPoint(), beesRoute.getEndPoint(),
                            currentPoint, time);
                }

                if (currentPoint != null && currentPoint.equals(beesRoute.getEndPoint())) {
                    beesRoute.setFinished(true);
                    finishedRoutesCount++;
                }

            }
            if (finishedRoutesCount == routesSize) {
                maxTime = time;
                time = 0;
                for (BeesRoute<WorkerBee> beesRoute : beesRoutes) {
                    Point oldStartPoint = new Point(beesRoute.getStartPoint().getX(), beesRoute.getStartPoint().getY());
                    beesRoute.setFinished(false);
                    beesRoute.setStartPoint(new Point(beesRoute.getEndPoint().getX(), beesRoute.getEndPoint().getY()));
                    beesRoute.setEndPoint(oldStartPoint);
                }
                finishedRoutesCount = 0;
                reversedCount++;
            }
            try {
                Thread.sleep(ONE_SECOND * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("\n");
            isMoving = (time != maxTime && reversedCount != 2);
            time++;

        }
    }
}
