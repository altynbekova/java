package com.altynbekova.bees_simulator.entity;

public interface IBehaviour{
   void fly();
   void search();
}
