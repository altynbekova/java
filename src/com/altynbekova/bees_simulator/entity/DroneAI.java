package com.altynbekova.bees_simulator.entity;

import com.altynbekova.bees_simulator.Runner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DroneAI extends BaseAI<DroneBee> {
    private List<DroneBee> droneBees;

    private int changeDirectionPeriodicity=5;
    private static final int VELOCITY = 1; //pixel per second
    private List<BeesRoute<DroneBee>> beesRoutes;
    private Random rnd=new Random();

    public DroneAI() {
    }

    public DroneAI(List<DroneBee> droneBees) {
        this.droneBees = droneBees;
        init();
    }

    private void init(){
        beesRoutes = new ArrayList<>(droneBees.size());
        for (DroneBee droneBee: droneBees) {
            BeesRoute<DroneBee> workersRoute = new BeesRoute<>();
            Point startPoint=new Point(rnd.nextInt(N + 1), rnd.nextInt(N + 1));
            Point endPoint= getEndPoint(startPoint);

            workersRoute.setBee(droneBee);
            workersRoute.setStartPoint(startPoint);
            workersRoute.setEndPoint(endPoint);
            workersRoute.setTotalTime(changeDirectionPeriodicity);

            beesRoutes.add(workersRoute);
        }

    }

    private Point getEndPoint(Point startPoint) {
        Direction direction=Direction.values()[rnd.nextInt(Direction.values().length)];
        Point endPoint = new Point();
        switch (direction.getValue()){
            case "Up":
                endPoint.setX(startPoint.getX());
                endPoint.setY(N);
                break;
            case "Right":
                endPoint.setX(N);
                endPoint.setY(startPoint.getY());
                break;
            case "Down":
                endPoint.setX(startPoint.getX());
                endPoint.setY(0);
                break;
            case "Left":
                endPoint.setX(0);
                endPoint.setY(startPoint.getY());
                break;
        }
        return endPoint;
    }


    //[left,up.right,down] every N seconds
    @Override
    protected void move() {
        boolean isMoving = true;
        int routesSize = beesRoutes.size();
        int time = 1;
        Point lastPoint=new Point();
        while (isMoving) {
            for (BeesRoute<DroneBee> beesRoute : beesRoutes) {
                int changeDirectionPeriodicity = beesRoute.getTotalTime();
                if (time > changeDirectionPeriodicity) {
                    System.out.println("\nDirection changed");
                    time=1;
                    beesRoute.setStartPoint(new Point(lastPoint.getX(), lastPoint.getY()));
                    beesRoute.setEndPoint(getEndPoint(beesRoute.getStartPoint()));
                }
                lastPoint=getCurrentPoint(beesRoute.getStartPoint(), beesRoute.getEndPoint(), time, VELOCITY);
                Runner.printCoordinates(beesRoute.getBee(), changeDirectionPeriodicity, beesRoute.getStartPoint(), beesRoute.getEndPoint(),
                        lastPoint, time);
            }
            try {
                Thread.sleep(ONE_SECOND*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("\n");
            time++;
            isMoving = routesSize > 0;
        }
    }

    private enum Direction{
        UP("Up"),
        RIGHT("Right"),
        DOWN("Down"),
        LEFT("Left");

        private String value;

        Direction(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
