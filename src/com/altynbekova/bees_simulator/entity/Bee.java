package com.altynbekova.bees_simulator.entity;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.UUID;

public abstract class Bee implements IBehaviour, Serializable {
    private UUID id;
    private int birthPeriodicity;
    private Date birthTime;
    private int lifeTime;
    private Type type;
    public static final transient Comparator<Bee> BIRTH_TIME_ORDER = new ByBirthTimeComparator();

    public Bee() {
    }

    public Bee(UUID id, int birthPeriodicity, Date birthTime) {
        this.id = id;
        this.birthPeriodicity = birthPeriodicity;
        this.birthTime = birthTime;
    }

    public Bee(UUID id, int birthPeriodicity, Date birthTime, int lifeTime) {
        this.id = id;
        this.birthPeriodicity = birthPeriodicity;
        this.birthTime = birthTime;
        this.lifeTime = lifeTime;
    }

    public Bee(int birthPeriodicity) {
        this.birthPeriodicity = birthPeriodicity;
    }

    public Bee(UUID id, int birthPeriodicity) {
        this.id = id;
        this.birthPeriodicity = birthPeriodicity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bee)) return false;

        Bee bee = (Bee) o;

        return getId() == bee.getId();
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    public int getBirthPeriodicity() {
        return birthPeriodicity;
    }

    public void setBirthPeriodicity(int birthPeriodicity) {
        this.birthPeriodicity = birthPeriodicity;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Date getBirthTime() {
        return birthTime;
    }

    public void setBirthTime(Date birthTime) {
        this.birthTime = birthTime;
    }

    public int getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(int lifeTime) {
        this.lifeTime = lifeTime;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void fly() {
        System.out.println("Bee is flying...");
    }

    @Override
    public void search() {
        System.out.println("Bee is searching...");
    }

    private static class ByBirthTimeComparator implements Comparator<Bee>, Serializable {
        @Override
        public int compare(Bee o1, Bee o2) {
            return o1.getBirthTime().compareTo(o2.getBirthTime());
        }
    }

    public enum Type{
        DRONE("Drone"),
        WORKER("Worker");

        private String value;

        Type() {
        }

        Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
