package com.altynbekova.bees_simulator.entity;

import com.altynbekova.bees_simulator.util.PropertyManager;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

public class DronesHabitat extends Habitat {
    private int droneBirthPeriodicity;
    private double droneBirthCoefficient;
    private double dronesPercentage;

    public DronesHabitat() {
    }

    public DronesHabitat(int droneBirthPeriodicity, double droneBirthCoefficient) {
        this.droneBirthPeriodicity = droneBirthPeriodicity;
        this.droneBirthCoefficient = droneBirthCoefficient;
    }

    @Override
    public DroneBee generateNewBee(int lifeTime) {
        UUID id = generateId();

        return new DroneBee(id, new Date(), droneBirthPeriodicity, lifeTime, droneBirthCoefficient);
    }

    @Override
    public void run() {
        try {
            int maxLifeTime=Integer.parseInt(PropertyManager.getInstance().getProperty("simulation.time"));
            dronesPercentage = (bees.size() == 0 ? 0 : (double) dronesAmount / bees.size());
            System.out.println("Drone bees percentage=" + dronesPercentage);

            if (dronesPercentage < droneBirthCoefficient) {

                DroneBee bee = generateNewBee(rnd.nextInt(maxLifeTime + 1));

                System.out.println(bee + " is generated\n");

                bees.add(bee);
                dronesAmount++;
            } else {
                System.out.println(new Date() + "\nCouldn't generate a drone bee\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
