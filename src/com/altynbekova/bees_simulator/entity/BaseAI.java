package com.altynbekova.bees_simulator.entity;

import com.altynbekova.bees_simulator.Runner;

public abstract class BaseAI<T extends Bee> extends Thread{
    //field size
    //protected static final int M=10;
    protected static final int N=10;
    protected static final int ONE_SECOND=1;

    protected final Point[] destinationPoints={
            new Point(0,0),
            new Point(0,N),
            new Point(N,N),
            new Point(N,0)
    };

    protected Point getCurrentPoint(Point p1, Point p2, int timeInSeconds, int velocity){
        int x, y;
        int dx = p2.getX() - p1.getX();
        int dy = p2.getY() - p1.getY();
        double dxNormalized = dx / Math.sqrt(dx * dx + dy * dy);
        double dyNormalized = dy / Math.sqrt(dx * dx + dy * dy);

        x=(int) Math.round(p1.getX() + dxNormalized * velocity * timeInSeconds);
        y= (int) Math.round(p1.getY() + dyNormalized * velocity * timeInSeconds);

        return new Point(x, y);
    }

    @Override
    public void run() {
        move();
    }

    protected abstract void move();
}
