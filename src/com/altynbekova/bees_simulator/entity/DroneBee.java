package com.altynbekova.bees_simulator.entity;

import java.util.Date;
import java.util.UUID;

public class DroneBee extends Bee {
    private double maxQuantityCoefficient;

    public DroneBee() {
    }

    public DroneBee(UUID id, Date birthTime, int birthPeriodicity, int lifeTime, double maxQuantityCoefficient) {
        super(id, birthPeriodicity, birthTime, lifeTime);
        this.maxQuantityCoefficient = maxQuantityCoefficient;
        this.setType(Type.DRONE);
    }

    public double getMaxQuantityCoefficient() {
        return maxQuantityCoefficient;
    }

    @Override
    public String toString() {
        return "DroneBee {id=" + getId() + ", birthTime=" + getBirthTime() + ", birthPeriodicity=" + getBirthPeriodicity() +
                ", lifeTime=" + getLifeTime() + " seconds, quantityCoefficient=" + getMaxQuantityCoefficient() + "%}";
    }
}
